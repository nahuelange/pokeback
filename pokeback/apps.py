from django.apps import AppConfig


class PokebackConfig(AppConfig):
    name = 'pokeback'

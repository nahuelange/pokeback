from uuid import uuid4

from django.db import models


class UUIDModel(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid4, null=False)

    class Meta:
        abstract = True


class PokemonColor(UUIDModel):
    name = models.CharField(max_length=255, blank=False, null=False,)
    color = models.CharField(max_length=6,)

    def __str__(self):
        return f'#{self.color}'


class Pokemon(UUIDModel):
    created_at = models.DateTimeField(auto_now=True,)
    name = models.CharField(max_length=255, blank=False, null=False,)
    color = models.ForeignKey(PokemonColor, on_delete=models.PROTECT,)

    class Meta:
        ordering = ['-created_at']

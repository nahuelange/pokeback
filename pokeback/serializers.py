from rest_framework.serializers import ModelSerializer, StringRelatedField

from .models import PokemonColor, Pokemon


class PokemonColorSerializer(ModelSerializer):
    class Meta:
        model = PokemonColor
        fields = '__all__'


class PokemonSerializer(ModelSerializer):
    color_code = StringRelatedField(source="color")

    class Meta:
        model = Pokemon
        fields = '__all__'

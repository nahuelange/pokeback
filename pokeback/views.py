from rest_framework.viewsets import ModelViewSet

from .models import PokemonColor, Pokemon
from .serializers import PokemonColorSerializer, PokemonSerializer


class PokemonColorViewset(ModelViewSet):
    pagination_class = None
    serializer_class = PokemonColorSerializer
    queryset = PokemonColor.objects.all()


class PokemonViewset(ModelViewSet):
    serializer_class = PokemonSerializer
    queryset = Pokemon.objects.all()

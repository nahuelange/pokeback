from rest_framework import routers

from .views import PokemonColorViewset, PokemonViewset

router = routers.SimpleRouter()
router.register(r'color', PokemonColorViewset)
router.register(r'', PokemonViewset)

urlpatterns = router.urls

# Pokeback

## Configuration

Copy the file `project/settings/local.py.dist` to `project.settings.local.py` and open it.

Fill the available variables.

## Deployment

First, you need to create a virtualenv, activate it and install requirements from requirements.txt

### Migrations

Launch the gcloud sql proxy replace `<sqlinstance>` by your connection string:

`./cloud_sql_proxy -instances "<sqlinstance>"=tcp:5432`

Temporary set your in your `local.py` the DATABASE's HOST to `localhost`

Then run:
```
$ python manage.py migrate
$ python manage.py loaddata pokeback/fixtures/colors.json
```

When done, you can stop `cloud_sql_proxy` and set back you database `HOST`

### Deploy

`gcloud deploy app`

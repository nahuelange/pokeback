import os

from .common import *

if 'GAE_INSTANCE' in os.environ:
    from .production import *
else:
    from .dev import *

try:
    from .local import *
except ImportError:
    pass
